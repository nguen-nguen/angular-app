import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {Observable, delay, throwError, catchError, retry} from 'rxjs';
import {IProduct} from '../models/product';
import {ErrorService} from './error.service';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})


export class ProductService implements OnInit{
  constructor(
    private http: HttpClient,
    private errorService : ErrorService
    ) {
  }
  products: IProduct[] = []
  getAll() : Observable<IProduct[]>{
    return this.http.get<IProduct[]>('https://fakestoreapi.com/products', {
      params : new HttpParams({
        fromObject : {
          limit : 5
        }
      })
    }).pipe(
      tap(products => this.products = products as IProduct[]),
      catchError(this.errorHandler.bind(this))
    ).pipe(
      delay( 2000 ),
      retry( 2 )
    )
  }

  createNewProduct(product : IProduct): Observable<IProduct> {
    return this.http.post<IProduct>('https://fakestoreapi.com/products', product)
      .pipe(tap((product) => this.products.push(product)))
  }
  private errorHandler(error : HttpErrorResponse){
    this.errorService.handleError(error.message)
    return throwError(() => error.message)
  }
  ngOnInit() {
  }
}
