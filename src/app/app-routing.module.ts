import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProductPageComponent} from './pages/product-page/product-page.component';
import {DescriptionComponent} from './pages/description/description.component';

const routes: Routes = [{
  path: "", component : ProductPageComponent,
}, {
  path : 'about', component : DescriptionComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
