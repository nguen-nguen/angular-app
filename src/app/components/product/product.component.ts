import {Component, Input} from "@angular/core";
import {IProduct} from "../../models/product";
import {products} from "../../data/products";
@Component({
  selector: "app-product",
  templateUrl : 'product.component.html'
})
export class ProductComponent{
  constructor() {}
  @Input() product: IProduct;
  details = false;
  protected readonly products = products;

};
