import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {ProductService} from '../../services/product.service';
import {ModalService} from '../../services/modal.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit{


  constructor(
    public productService : ProductService,
    public  modalService : ModalService
  ) {
  }
  form = new FormGroup({
    title: new FormControl<string>('', [
      Validators.required,
      Validators.minLength(6)
    ])
  })
  submit(){

    this.productService.createNewProduct({
      title: String(this.form.value.title),
      price: 13.5,
      description: 'lorem ipsum set',
      image: 'https://i.pravatar.cc',
      category: 'electronic',
      rating:{
        rate: 0.6,
        count: 56,
      }
    }).subscribe(() =>{
      this.modalService.close()
    } )
  }
  get title(){
    return this.form.controls.title as FormControl
  }
  ngOnInit() {
  }
}
