import { Directive, ElementRef, OnInit, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appFocus]'
})
export class FocusDirective implements OnInit, AfterViewInit {
  constructor(private el: ElementRef) { }
  ngOnInit() {
    this.el.nativeElement.focus()
  }
  ngAfterViewInit() {
    // this.el.nativeElement.focus()
  }
}
