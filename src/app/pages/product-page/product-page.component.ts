import { Component } from '@angular/core';
import {ProductService} from '../../services/product.service';
import {ModalService} from '../../services/modal.service';
import {Observable} from 'rxjs';
import {IProduct} from '../../models/product';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.css']
})
export class ProductPageComponent {
  constructor(
    public productService : ProductService,
    public modalService: ModalService
  ) {}


  products$: Observable<IProduct[]>;
  loading = false
  term=''

  ngOnInit(): void {
    this.loading = true
    this.productService.getAll().subscribe(() => this.loading = false)
  }

}
